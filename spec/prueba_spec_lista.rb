require 'spec_helper'


describe Lista do
    
    before :each do
        
        
        
          #  @lib1 = Bbl.new(['Dave Thomas', 'Andy Hunt', 'Chad Fowler'],'The Pragmatic Programmers Guide','serie 3','The Facets of Ruby','Pragmatic Bookshelf; 4 edition','July 7, 2013',3459)
        @lib1 = Bbl.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"], "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide. (The Facets of Ruby)", "Pragmatic Bookshelf", "", 4, "(July 7, 2013)", ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"])
        @lib2 = Bbl.new(["Scott Chacon"], "Pro Git 2009th Edition.(Pro)", "Apress", "", 2009, "(August 27, 2009)", ["ISBN-13: 978-1430218333","ISBN-10: 1430218339"])
        @lib3 = Bbl.new(["David Flanagan","Yukihiro Matsumoto"],"The Ruby Programming Language.", "O’Reilly Media", "", 1, "(February 4, 2008)",["ISBN-10: 0596516177","ISBN-13: 978-0596516178"])
        @lib4 = Bbl.new(["David Chelimsky","Dave Astels","Bryan Helmkamp","Dan North","Zach Dennis","Aslak Hellesoy"],"The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends (The Facets of Ruby).", "PragmaticBookshelf", "", 1, "(December 25, 2010)",["ISBN-10: 1934356379","ISBN-13: 978-1934356371"])
        @lib5 = Bbl.new(["Richard E"],"Silverman Git Pocket Guide", "O’Reilly Media", "", 1, "(August 2, 2013)",["ISBN-10: 1449325866","ISBN-13: 978-1449325862"])
    
        
        @list1=Lista.new()
        @nodo1 = Node.new(@lib1,nil)
        @nodo2 = Node.new(@lib1,nil)
        @nodo3 = Node.new(@lib1,nil)
        @nodo4 = Node.new(@lib1,nil)
        @nodo5 = Node.new(@lib1,nil)
        @nodo6 = Node.new(@lib2,nil)
    
        end
    
end
    describe "desarrollo práctica" do
    it "es de la clase" do
            expect(@list1.is_a? Lista).to be true
            expect(@list1.instance_of?Lista).to be true
            expect(@nodo1.instance_of?Lista).not_to be true
            expect(@lib1.is_a? Bbl).to be true
        end

    
    it "Debe existir un nodo en la lista" do
        @list1.insertar_nodo(@nodo1)
        @list1.insertar_nodo(@nodo2)
        
        expect(@list1.lista_vacia()).not_to be true
    
        aux = @list1.extraer_inicio()
        expect(aux.value.to_s).not_to be nil
        expect(aux.next).not_to be nil
    end
    
     it '#Se extrae el primer elemento de la lista' do
      expect(@list1.to_s()).to eq "\n Autores: David Flanagan, Yukihiro Matsumoto\n Título: The Ruby Programming Language\n Editorial: O’Reilly Media\n Edición: 1 edition\n Fecha: February4, 2008\n Código: ISBN-10: 0596516177, ISBN-13: 978-0596516178"
       expect(@list1.extraer_inicio()).respond_to?("hola",false)
   en

  
  
    it '#Se puede insertar un elemento' do
      expect(@list2.insertar_nodo()).to eq "\n Autores: Dave Thomas, Andy Hunt, Chad Fowler\n Título: The Pragmatic Programmers Guide\n Serie: The Facets of Ruby\n Editorial: Pragmatic Bookshelf\n Edición: 4 edition\n Fecha: July 7, 2013\n Código: ISBN-13: 978-1937785499, ISBN-10: 1937785491"
    end
  
  
  
    it '#Se pueden insertar varios elementos' do
      expect(@list4.to_s()).to eq "\n Autores: David Chelimsky, Dave Astels, Bryan Helmkamp, Dan North, Zach Dennis, Aslak Hellesoy\n Título: The RSpecBook: Behaviour Driven Development with RSpec, Cucumber, and Friends\n Editorial: The Face,ts of Ruby\n Edición: PragmaticBookshelf\n Fecha: 1 editionDecember 25, 2010\n Código: ISBN-10: 1934356379, ISBN-13: 978-1934356371 \n Autores: Richard E. Silverman\n Título: Git Pocket Guide\n Editorial: O’Reilly Media\n Edición: 1 edition\n Fecha: August 2, 2013\n Código: ISBN-10: 1449325866, ISBN-13: 978-1449325862"
    end
    
    it '#Debe existir una Lista con su cabeza' do
      expect{@list3.extraer_inicio()}.to raise_error( RuntimeError,"No se puede extraer porque debe existir una lista con su cabeza")
    end
    
    
    end
  